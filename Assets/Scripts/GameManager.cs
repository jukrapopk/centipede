using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering.PostProcessing;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [Header("Prefabs")]
    [SerializeField]
    private GameObject playerPrefab;

    private GameObject _player;

    [SerializeField]
    private GameObject centipedePrefab;

    [SerializeField]
    private GameObject mushroomPrefab;

    [Header("Game Components")]
    [SerializeField]
    private GameObject menu;

    [SerializeField]
    private Camera mainCamera;

    [SerializeField]
    private TMP_Text textMeshTitle;

    [SerializeField]
    private TMP_Text textMeshScore;

    [SerializeField]
    private TMP_Text textMeshLives;

    [SerializeField]
    private PostProcessVolume postProcessVolume;

    [Header("Game Parameters")]
    [SerializeField]
    private bool singleRound = true;

    [SerializeField]
    private int maxLives = 3;

    private int _lives = 3;

    [SerializeField]
    private float playerMovementSpeed = 20;

    [SerializeField]
    public float bulletMovementSpeed = 8;

    [Header("Map Parameters")]
    [SerializeField]
    private int width = 32;

    [SerializeField]
    private int height = 32;

    [SerializeField]
    public int playableAreaHeight = 5;

    public const float PositionUnit = 0.08f;

    [HideInInspector]
    public float topBorderY;

    [HideInInspector]
    public float leftBorderX;

    [HideInInspector]
    public Vector2Int mapDimensions;

    [Header("Centipede Parameters")]
    [SerializeField]
    private int startCentipedeLength = 15;

    private int _centipedeLength;

    [SerializeField]
    private float startCentipedeMovementSpeed = 10;

    private float _centipedeMovementSpeed;

    [SerializeField]
    public int topMargin = 2;

    [Header("Mushroom Parameters")]
    [SerializeField]
    private int mushroomAmount = 88;

    [SerializeField]
    private int mushroomHitToDestroy = 3;

    [SerializeField]
    private int mushroomFreeTopMargin = 2;

    [SerializeField]
    private int mushroomFreeBottomMargin = 2;

    private int Lives
    {
        get => _lives;
        set
        {
            _lives = Math.Max(value, 0);
            textMeshLives.SetText("x" + _lives);
        }
    }

    private int _score;

    public int Score
    {
        get => _score;
        set
        {
            _score = value;
            textMeshScore.SetText(value.ToString());
        }
    }

    private GameState _currentGameState;
    private PlayerInput _menuPlayerInput;

    private List<CentipedeInfo> _centipedes;
    private List<MushroomInfo> _mushrooms;

    #region Lifecycle

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        _menuPlayerInput = GetComponent<PlayerInput>();
    }

    private void Start()
    {
        CalculateGridMap();
        _currentGameState = GameState.Menu;
        _centipedes = new List<CentipedeInfo>();
        _mushrooms = new List<MushroomInfo>();
        Lives = maxLives;
        _centipedeLength = startCentipedeLength;
        _centipedeMovementSpeed = startCentipedeMovementSpeed;
        ChangeColor();
        PopulateCentipedes();
        PopulateMushrooms();
    }

    private void OnPlay()
    {
        if (_currentGameState == GameState.Menu)
        {
            singleRound = true;
            ClearAllCentipedes();
            StartGame();
        }
    }

    private void OnPlayEndless()
    {
        if (_currentGameState == GameState.Menu)
        {
            singleRound = false;
            ClearAllCentipedes();
            StartGame();
        }
    }

    #endregion

    #region Game State

    private void StartGame()
    {
        _menuPlayerInput.enabled = false;
        menu.SetActive(false);
        _currentGameState = GameState.InGame;

        Lives = maxLives - 1;
        Score = 0;

        _centipedeLength = startCentipedeLength;
        _centipedeMovementSpeed = startCentipedeMovementSpeed;

        ChangeColor();

        PopulateCentipedes();
        PopulateMushrooms();
        RespawnPlayer();
    }

    private void EndGame()
    {
        Invoke(nameof(DisableMenuPlayerInput), 0.8f);
        textMeshTitle.SetText("Game Over");
        menu.SetActive(true);
        _currentGameState = GameState.Menu;
    }

    private void ChangeColor()
    {
        postProcessVolume.profile.TryGetSettings(out ColorGrading colorGrading);
        float random;
        do
        {
            random = Random.Range(0, 4);
        } while (Math.Abs(colorGrading.hueShift.value - random * -40) < 1);

        colorGrading.hueShift.value = random * -40;
    }

    private void DisableMenuPlayerInput()
    {
        _menuPlayerInput.enabled = true;
    }

    #endregion

    #region Grid Map

    private void CalculateGridMap()
    {
        width = width % 2 == 0 ? width : width - 1;
        height = height % 2 == 0 ? height : height - 1;

        mainCamera.orthographicSize = Math.Max(width, height) * PositionUnit * 0.5f;

        topBorderY = height / 2.0f * PositionUnit;
        leftBorderX = -width / 2.0f * PositionUnit;
        mapDimensions = new Vector2Int(width, height);
    }

    public Vector2 GetGridPosition(int x, int y)
    {
        return new Vector2(leftBorderX + (x * PositionUnit) + (PositionUnit / 2.0f),
            topBorderY - (y * PositionUnit) - (PositionUnit / 2.0f));
    }

    #endregion

    #region Player

    public void PlayerDead()
    {
        if (Lives <= 0)
        {
            EndGame();
        }
        else
        {
            Lives--;
            Invoke(nameof(ClearAllCentipedes), 1.0f);
            Invoke(nameof(PopulateCentipedes), 2.0f);
            Invoke(nameof(RespawnPlayer), 1.6f);
        }
    }

    private void RespawnPlayer()
    {
        foreach (var o in GameObject.FindGameObjectsWithTag("Player"))
        {
            Destroy(o);
        }

        var playerGridPosition = new Vector2Int(mapDimensions.x / 2, mapDimensions.y - 1);
        _player = Instantiate(playerPrefab, GetGridPosition(playerGridPosition.x, playerGridPosition.y), Quaternion.identity);
        _player.GetComponent<PlayerController>().gridPosition = playerGridPosition;
        _player.GetComponent<PlayerController>().movementSpeed = playerMovementSpeed;
    }

    #endregion

    #region Centipede

    private void PopulateCentipedes()
    {
        CentipedeController nextCentipede = null;
        for (int i = -_centipedeLength + 1; i <= 0; i++)
        {
            var temp = Instantiate(centipedePrefab, GetGridPosition(i, topMargin), Quaternion.identity).GetComponent<CentipedeController>();
            temp.gridPosition = new Vector2Int(i, topMargin);
            temp.nextCentipede = nextCentipede;
            temp.movementSpeed = _centipedeMovementSpeed;
            nextCentipede = temp;

            if (i < 0)
            {
                temp.Mode = CentipedeMode.Body;
            }
        }
    }

    private void ClearAllCentipedes()
    {
        foreach (var o in GameObject.FindGameObjectsWithTag("Centipede"))
        {
            Destroy(o);
        }

        _centipedes.Clear();
    }

    public void UpdateCentipedeGridPosition(int instanceID, Vector2Int gridPosition)
    {
        if (_centipedes.Any(centipede => centipede.InstanceId == instanceID))
        {
            _centipedes.Find(centipede => centipede.InstanceId == instanceID).GridPosition = gridPosition;
        }
        else
        {
            _centipedes.Add(new CentipedeInfo(gridPosition, instanceID));
        }
    }

    public bool CheckCentipedeAtGridPosition(Vector2Int gridPosition)
    {
        return _centipedes.Any(centipede => centipede.GridPosition == gridPosition);
    }

    public void DestroyCentipede(int instanceID)
    {
        _centipedes.Remove(_centipedes.Find(centipede => centipede.InstanceId == instanceID));
        if (_centipedes.Count <= 0)
        {
            if (singleRound)
            {
                EndGame();
            }
            else
            {
                _centipedeLength += 2;
                _centipedeMovementSpeed += 0.5f;
                Invoke(nameof(PopulateCentipedes), 0.6f);
                Invoke(nameof(ChangeColor), 0.6f);
            }
        }
    }

    #endregion

    #region Mushroom

    private void PopulateMushrooms()
    {
        ClearAllMushrooms();
        int newY = mapDimensions.y - (mushroomFreeTopMargin + mushroomFreeBottomMargin);
        mushroomAmount = Math.Min(mushroomAmount, mapDimensions.x * newY);
        var temp = Utility.GetRandomNumbers(mapDimensions.x * newY, mushroomAmount);
        var tempList = new List<MushroomInfo>();
        foreach (int i in temp)
        {
            int x = i / newY;
            int y = i - ((x * newY) - mushroomFreeTopMargin);
            var tempMushroom = Instantiate(mushroomPrefab, GetGridPosition(x, y), Quaternion.identity);
            tempMushroom.GetComponent<MushroomController>().maxHp = mushroomHitToDestroy;
            tempMushroom.GetComponent<MushroomController>().hp = mushroomHitToDestroy;
            tempList.Add(new MushroomInfo(new Vector2Int(x, y), tempMushroom));
        }

        _mushrooms = tempList;
    }

    private void ClearAllMushrooms()
    {
        foreach (var o in GameObject.FindGameObjectsWithTag("Mushroom"))
        {
            Destroy(o);
        }

        _mushrooms.Clear();
    }

    public void AddMushroom(Vector2Int gridPosition)
    {
        var temp = Instantiate(mushroomPrefab, GetGridPosition(gridPosition.x, gridPosition.y), Quaternion.identity);
        _mushrooms.Add(new MushroomInfo(gridPosition, temp));
    }

    public bool CheckMushroomAtGridPosition(Vector2Int gridPosition)
    {
        if (gridPosition.x == 0 && gridPosition.y == 0)
            return false;
        return _mushrooms.Any(mushroom => mushroom.GridPosition == gridPosition);
    }

    public void DestroyMushroom(int instanceID)
    {
        _mushrooms.Remove(_mushrooms.Find(mushroom => mushroom.MushroomObject.GetInstanceID() == instanceID));
    }

    #endregion
}

public enum GameState
{
    Menu,
    InGame
}