using UnityEngine;

public class MushroomController : MonoBehaviour
{
    public int maxHp = 3;

    public int hp;

    [SerializeField]
    private SpriteRenderer sprite;

    private void Awake()
    {
        hp = maxHp;
    }

    private void OnDamage()
    {
        hp--;
        var tempColor = sprite.color;
        tempColor.a = (float) hp / (float) maxHp;
        sprite.color = tempColor;
        if (hp == 0)
        {
            GameManager.Instance.DestroyMushroom(gameObject.GetInstanceID());
            GameManager.Instance.Score += 1;
            Destroy(gameObject);
        }
    }
}

public class MushroomInfo
{
    public Vector2Int GridPosition;
    public readonly GameObject MushroomObject;

    public MushroomInfo(Vector2Int gridPosition, GameObject mushroomObject)
    {
        GridPosition = gridPosition;
        MushroomObject = mushroomObject;
    }
}