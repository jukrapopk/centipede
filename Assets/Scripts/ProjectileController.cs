using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 8;

    private Vector2 _direction;

    private Rigidbody2D _rigidbody2D;

    private bool _hasCollided;

    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        movementSpeed = GameManager.Instance.bulletMovementSpeed;
        _direction = new Vector2(0, movementSpeed);
    }

    private void Update()
    {
        transform.position = _rigidbody2D.position + _direction * (GameManager.PositionUnit * Time.deltaTime);
        if (transform.position.y > GameManager.Instance.topBorderY)
        {
            Destroy(gameObject, 1);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!_hasCollided && other.CompareTag("Centipede") || other.CompareTag("Mushroom"))
        {
            _direction = Vector2.zero;
            _rigidbody2D.velocity = Vector2.zero;
            other.gameObject.SendMessage("OnDamage", SendMessageOptions.DontRequireReceiver);
            _hasCollided = true;
            Destroy(gameObject);
        }
    }
}