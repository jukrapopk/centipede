using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    public float movementSpeed = 16;

    [SerializeField]
    private Transform bulletSpawningPoint;

    [SerializeField]
    private GameObject bulletPrefab;

    public Vector2Int gridPosition;
    private float _nextMoveTime;

    private bool _hasCollided;

    private Vector2Int _direction;
    private Vector2Int _input;
    private Vector2Int _directionMultiplier = new Vector2Int(1, -1);

    private int _limitLeftX;
    private int _limitRightX;
    private int _limitTopY;
    private int _limitBottomY;

    private void Start()
    {
        _limitLeftX = 0;
        _limitRightX = GameManager.Instance.mapDimensions.x - 1;
        _limitTopY = GameManager.Instance.mapDimensions.y - GameManager.Instance.playableAreaHeight;
        _limitBottomY = GameManager.Instance.mapDimensions.y - 1;
    }

    private void OnDirection(InputValue value)
    {
        _input = Vector2Int.RoundToInt(value.Get<Vector2>());
    }

    private void OnShoot()
    {
        Shoot();
    }

    private void Update()
    {
        if (_input.magnitude > 0 && Time.time >= _nextMoveTime)
        {
            _direction = _input;
            if (GameManager.Instance.CheckMushroomAtGridPosition(new Vector2Int(gridPosition.x + _direction.x, gridPosition.y)))
            {
                _direction.x = 0;
            }

            if (GameManager.Instance.CheckMushroomAtGridPosition(new Vector2Int(gridPosition.x, gridPosition.y - _direction.y)))
            {
                _direction.y = 0;
            }

            if (GameManager.Instance.CheckMushroomAtGridPosition(gridPosition + _direction * _directionMultiplier))
            {
                _direction = Vector2Int.zero;
                if (!GameManager.Instance.CheckMushroomAtGridPosition(new Vector2Int(gridPosition.x + _direction.x, gridPosition.y)))
                {
                    _direction.x = _input.x;
                }
                else if (!GameManager.Instance.CheckMushroomAtGridPosition(new Vector2Int(gridPosition.x, gridPosition.y - _direction.y)))
                {
                    _direction.y = _input.y;
                }
            }

            gridPosition += _direction * _directionMultiplier;
            gridPosition = new Vector2Int(Mathf.Clamp(gridPosition.x, _limitLeftX, _limitRightX), Mathf.Clamp(gridPosition.y, _limitTopY, _limitBottomY));

            _nextMoveTime = Time.time + 1 / movementSpeed;
        }

        var position = GameManager.Instance.GetGridPosition(gridPosition.x, gridPosition.y);
        float step = GameManager.PositionUnit * Time.deltaTime * movementSpeed;
        var temp = Vector3.MoveTowards(transform.position, position, step);
        transform.position = temp;
    }

    private void Shoot()
    {
        Instantiate(bulletPrefab, bulletSpawningPoint.position, bulletSpawningPoint.rotation);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!_hasCollided && other.CompareTag("Centipede"))
        {
            _hasCollided = true;
            GameManager.Instance.PlayerDead();
            Destroy(gameObject);
        }
    }
}