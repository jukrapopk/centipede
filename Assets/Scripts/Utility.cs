using System.Collections.Generic;
using Random = UnityEngine.Random;

public static class Utility
{
    public static List<int> GetRandomNumbers(int range, int amount)
    {
        amount = amount > range ? range : amount;
        var list = new List<int>();
        for (int i = 0; i < amount; i++)
        {
            int temp;
            do
            {
                temp = Random.Range(0, range);
            } while (list.Contains(temp));

            list.Add(temp);
        }

        return list;
    }
}