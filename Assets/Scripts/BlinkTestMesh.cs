using TMPro;
using UnityEngine;

public class BlinkTestMesh : MonoBehaviour
{
    [SerializeField]
    private float blinkInterval = 1;

    private TMP_Text _textMesh;

    private void Awake()
    {
        _textMesh = GetComponent<TMP_Text>();
    }

    private void Update()
    {
        var temp = _textMesh.color;
        temp.a = Mathf.PingPong(Time.time / blinkInterval, 1);
        _textMesh.color = temp;
    }
}