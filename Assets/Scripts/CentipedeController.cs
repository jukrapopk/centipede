using UnityEngine;

public class CentipedeController : MonoBehaviour
{
    [SerializeField]
    public float movementSpeed = 10f;

    [SerializeField]
    private SpriteRenderer sprite;

    [SerializeField]
    private Sprite headSprite;

    [SerializeField]
    private Sprite bodySprite;

    public Vector2Int gridPosition;
    private float _nextMoveTime;

    private Vector2Int _horizontalDirection = Vector2Int.right;
    private Vector2Int _verticalDirection = Vector2Int.down;
    private Vector2Int _directionProcessing = new Vector2Int(1, -1);

    private int _limitLeftX;
    private int _limitRightX;
    private int _limitTopY;
    private int _limitBottomY;

    public CentipedeController nextCentipede;
    private CentipedeMode _mode = CentipedeMode.Head;

    public CentipedeMode Mode
    {
        set
        {
            _mode = value;
            sprite.sprite = _mode == CentipedeMode.Head ? headSprite : bodySprite;
        }
        get => _mode;
    }

    private void Start()
    {
        _limitLeftX = 0;
        _limitRightX = GameManager.Instance.mapDimensions.x - 1;
        _limitTopY = GameManager.Instance.topMargin;
        _limitBottomY = GameManager.Instance.mapDimensions.y - 1;
    }

    private void Update()
    {
        if (Time.time >= _nextMoveTime)
        {
            if (nextCentipede)
            {
                nextCentipede.gridPosition = gridPosition;
                nextCentipede._verticalDirection = _verticalDirection;
                nextCentipede._horizontalDirection = _horizontalDirection;
            }

            if (_mode == CentipedeMode.Head)
            {
                if (GameManager.Instance.CheckMushroomAtGridPosition(gridPosition + _horizontalDirection) ||
                    GameManager.Instance.CheckCentipedeAtGridPosition(gridPosition + _horizontalDirection) ||
                    gridPosition.x == _limitRightX && _horizontalDirection == Vector2.right ||
                    gridPosition.x == _limitLeftX && _horizontalDirection == Vector2.left)
                {
                    Drop();
                    _horizontalDirection *= Vector2Int.left;
                }
                else
                {
                    gridPosition += _horizontalDirection * _directionProcessing;
                }
            }

            sprite.gameObject.transform.rotation = Quaternion.Euler(0, 0, _horizontalDirection == Vector2Int.left ? 0 : 180);

            GameManager.Instance.UpdateCentipedeGridPosition(gameObject.GetInstanceID(), gridPosition);
            _nextMoveTime = Time.time + 1 / movementSpeed;
        }

        var position = GameManager.Instance.GetGridPosition(gridPosition.x, gridPosition.y);
        float step = GameManager.PositionUnit * Time.deltaTime * movementSpeed;
        var temp = Vector3.MoveTowards(transform.position, position, step);
        transform.position = temp;
    }

    private void Drop()
    {
        if (gridPosition.y - _verticalDirection.y > _limitBottomY || gridPosition.y - _verticalDirection.y < _limitTopY)
        {
            _verticalDirection *= Vector2Int.down;
        }

        gridPosition -= _verticalDirection;
    }

    private void OnDamage()
    {
        if (nextCentipede)
        {
            nextCentipede.Mode = CentipedeMode.Head;
            nextCentipede.Drop();
            nextCentipede._horizontalDirection *= Vector2Int.left;
        }

        GameManager.Instance.DestroyCentipede(gameObject.GetInstanceID());
        GameManager.Instance.AddMushroom(gridPosition);
        GameManager.Instance.Score += 100;
        Destroy(gameObject);
    }
}

public class CentipedeInfo
{
    public Vector2Int GridPosition;
    public readonly int InstanceId;

    public CentipedeInfo(Vector2Int gridPosition, int instanceId)
    {
        GridPosition = gridPosition;
        this.InstanceId = instanceId;
    }
}

public enum CentipedeMode
{
    Head,
    Body
}